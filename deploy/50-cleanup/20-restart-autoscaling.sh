#!/bin/bash

### 110-m2-remove-maintenance.sh: this disables maintenance mode (if necessary)

if [ -z ${OUTPUT_DIR+x} ]; then
    echo "This depends on being executed by all.sh"
    exit
fi

if [[ -x "/usr/share/stratus/cli" ]]; then
    logvalue "Running autoscaling reinit"

    set -x;
    
    /usr/share/stratus/cli autoscaling.reinit;
    
    sleep 150s;

    /usr/share/stratus/cli cache.cloudfront.invalidate;
    /usr/share/stratus/cli cache.varnish.clear;

    redis-cli -h redis flushall && redis-cli -h redis-config-cache -p 6381 flushall;

    cd ${OUTPUT_DIR};
    host=$($PHP bin/magento config:show "web/secure/base_url")

    logvalue "Here's the host: "$host

    status_code=$(curl -kI --header "Host: $host" --write-out %{http_code} --silent --output /dev/null 'https://nginx/')

    logvalue "Status code: "$status_code

    if [[ "$status_code" -ne 200 ]] ; then
        echo "Site not active $status_code please push script again"
    else
        echo "\e[41m****Activity Completed please visit store and test****";
    fi
fi
