#!/bin/bash

# Detect Stratus Environment -> If no DB changes required, use ZeroDowntime feature
if [[ $RUN_DB_UPGRADE = 0 ]]; then
  [[ -x /usr/share/stratus/cli ]] && /usr/share/stratus/cli zerodowntime.init
fi