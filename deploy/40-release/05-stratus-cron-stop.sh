#!/bin/bash

# Detect Stratus environment -> Disable Cron Jobs
[[ -x /usr/share/stratus/cli ]] && /usr/share/stratus/cli crons.stop