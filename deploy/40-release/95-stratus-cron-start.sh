#!/bin/bash

# Detect Stratus environment -> Reindex Missed Indexer Changes & Enable Cron Jobs
if [[ -x /usr/share/stratus/cli ]]; then
  cd ${OUTPUT_DIR}
  n98-magerun2 sys:cron:run indexer_reindex_all_invalid;
  n98-magerun2 sys:cron:run indexer_update_all_views;

  /usr/share/stratus/cli crons.start
fi