#!/bin/bash

# Detect Stratus Environment -> If no DB changes required, swap source pod for zero downtime
if [[ $RUN_DB_UPGRADE = 0 ]]; then
  [[ -x /usr/share/stratus/cli ]] && /usr/share/stratus/cli zerodowntime.switch
fi