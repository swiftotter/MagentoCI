#!/bin/bash

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
source $HOME/.nvm/nvm.sh
nvm install $NODE_VERSION
nvm use $NODE_VERSION
npm install -g yarn

### 50-m2-gulp.sh: this file runs the Snowdog Apps gulp commands
node -v
ls -alh "${BUILD_DIR}/"
if [ -f "${BUILD_DIR}/package.json" ]; then
    (cd "${BUILD_DIR}/" \
        && yarn install \
        && yarn run prod \
    )
elif [ -f "${CHECKOUT_DIR}/package.json" ]; then
    (cd "${CHECKOUT_DIR}/" \
        && yarn install \
        && yarn run prod \
    )
fi
