<?php
$path = __DIR__;

foreach ($argv as $value) {
    if (stripos($value, '--path') === -1) {
        continue;
    }

    $path = str_ireplace('--path', '', $value);
    $path = trim($path, '= ');
}

$path = rtrim($path, '/');
$file = $path . '/composer.json';

if (!file_exists($file)) {
    echo "No composer.json found here: " . $file;
    exit(1);
}

$composerContents = file_get_contents($file);
$lines = explode("\n", $composerContents);

$inRequire = false;
foreach ($lines as $row => $line) {
    $shouldRemoveLine = false;

    if ($inRequire) {
        if (stripos($line, "magento/product-enterprise-edition") !== false
            || stripos($line, "magento/product-community-edition") !== false) {
            $lines[$row] = rtrim($line, ",");
            continue;
        }

        $shouldRemoveLine = true;
    }

    if (strpos($line, '"require"') !== false) {
        $inRequire = true;
        $shouldRemoveLine = false;
    }

    if (strpos($line, "}") !== false) {
        $inRequire = false;
        $shouldRemoveLine = false;
    }


    if ($shouldRemoveLine) {
        unset($lines[$row]);
    }
}

file_put_contents($file, implode("\n", $lines));
