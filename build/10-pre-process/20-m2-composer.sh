#!/bin/bash

### 10-composer.sh: this file installs the composer dependencies and the base Magento system

logvalue "Installing composer dependencies"

set -x

if [ -f "${CHECKOUT_DIR}/auth.json" ]; then
    yes | cp -rf ${CHECKOUT_DIR}/auth.* ${BUILD_DIR}
fi

if [ -f "${CHECKOUT_DIR}/src/composer.json" ]; then
    yes | cp -rf ${CHECKOUT_DIR}/src/composer.* ${BUILD_DIR}
fi

if [ -f "${CHECKOUT_DIR}/composer.json" ]; then
    rm -rf ${BUILD_DIR}/composer.*
    rm -rf ${BUILD_DIR}/vendor
    yes | cp -rf ${CHECKOUT_DIR}/composer.* ${BUILD_DIR}
fi

if [ -d "${CHECKOUT_DIR}/patches" ]; then
    mkdir -p ${BUILD_DIR}/patches
    yes | cp -rf ${CHECKOUT_DIR}/patches/* ${BUILD_DIR}/patches
fi

if [ -d "${CHECKOUT_DIR}/patch" ]; then
    mkdir -p ${BUILD_DIR}/patch
    yes | cp -rf ${CHECKOUT_DIR}/patch/* ${BUILD_DIR}/patch
fi

ls -alh ${BUILD_DIR}

if [ -f "${BUILD_DIR}/composer.json" ]; then
    logvalue "Executing composer install"
    ADDITIONS=""
    if [[ ${TESTMODE} == 0 ]] || [[ -z ${TESTMODE+x} ]]; then
        ADDITIONS="--no-dev"
    fi
    COMPOSER_PATH="$(which composer)"
    (
     cd ${BUILD_DIR} && $COMPOSER_CMD clearcache \
        && $COMPOSER_CMD install $ADDITIONS \
        --ignore-platform-reqs \
        --no-interaction \
        --no-progress \
        --no-suggest \
        --prefer-dist \
        --optimize-autoloader \
        --quiet
    )

    ls -alh ${BUILD_DIR}/vendor
fi

ls -alh ${BUILD_DIR}
chmod -R 777 ${BASE}

set +x
