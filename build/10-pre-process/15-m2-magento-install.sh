#!/bin/bash

### 20-m1-build-test.sh: checks to ensure the build was successful.

logvalue "Initial Composer install"

#!/bin/bash

### 5-m2-magento-install.sh: this file installs a stripped-back version of composer. It will be completed later.

logvalue "Installing composer dependencies"

set -x

if [ -f "${CHECKOUT_DIR}/auth.json" ]; then
    cp ${CHECKOUT_DIR}/auth.* ${BUILD_DIR}
fi

if [ -f "${CHECKOUT_DIR}/composer.json" ]; then
    cp ${CHECKOUT_DIR}/composer.* ${BUILD_DIR}
fi

if [ -d "${CHECKOUT_DIR}/patches" ]; then
    mkdir -p ${BUILD_DIR}/patches
    cp -r ${CHECKOUT_DIR}/patches/* ${BUILD_DIR}/patches
fi

if [ -d "${CHECKOUT_DIR}/patch" ]; then
    mkdir -p ${BUILD_DIR}/patch
    cp -r ${CHECKOUT_DIR}/patch/* ${BUILD_DIR}/patch
fi



DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
$PHP $DIR/clean-composer.php --path=${BUILD_DIR}
rm ${BUILD_DIR}/composer.lock

if [ -f "${BUILD_DIR}/composer.json" ]; then
    logvalue "Executing composer install"
    ADDITIONS=""
    if [[ ${TESTMODE} == 0 ]] || [[ -z ${TESTMODE+x} ]]; then
        ADDITIONS="--no-dev"
    fi
    COMPOSER_PATH="$(which composer)"
    (
     cd ${BUILD_DIR} && $COMPOSER_CMD clearcache \
        && $COMPOSER_CMD install $ADDITIONS \
        --ignore-platform-reqs \
        --no-interaction \
        --no-progress \
        --no-suggest \
        --prefer-dist \
        --optimize-autoloader
    )
fi



ls -alh ${BUILD_DIR}
chmod -R 777 ${BASE}

logvalue "Installing Magento"

cd ${BUILD_DIR}

chmod 777 .
mkdir -p ${BUILD_DIR}/pub/media
chmod -R 777 ${BUILD_DIR}/pub/media
chmod +x bin/magento

rsync_cmd="rsync --recursive --copy-links --specials --exclude='.git/' --exclude='.gitignore' --exclude='*README*'  -v --stats --progress "
if [[ -d "${CHECKOUT_DIR}/home/override" ]]; then
  ${rsync_cmd} ${CHECKOUT_DIR}/home/override/* ${BUILD_DIR}
  rm -rf ${BUILD_DIR}/pub/override
fi

if grep  -qe "magento\/product\-.*2\.4" "${BUILD_DIR}/composer.json"; then
  ELASTICSEARCH="--search-engine=elasticsearch7 --elasticsearch-host=${ELASTICSEARCH_HOST} --elasticsearch-port=9200"
fi

${MAGENTO_CMD} setup:install \
    --admin-firstname="Magento" \
    --admin-lastname="Builder" \
    --admin-email="joseph@swiftotter.com" \
    --admin-user="${MAGE_ADMIN_USER}" \
    --admin-password="${MAGE_ADMIN_PASSWORD}" \
    --db-host="${MYSQL_HOST}" \
    --db-name="${MYSQL_DATABASE}" \
    --db-user="${MYSQL_USER}" \
    --db-password="${MYSQL_PASSWORD}" \
    $ELASTICSEARCH
