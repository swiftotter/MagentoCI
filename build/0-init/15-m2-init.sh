#!/bin/bash

### 15-m2-init.sh: this file initializes additional variable for Magento 2

logvalue "Initializing M2 variables"
logvalue "Magento version: ${MAGENTO_VERSION}"

set -x

if [[ $MAGENTO_VERSION == "2."* ]]; then
    ## Composer vendor directory
    export MAGENTO_CMD="$PHP_CMD bin/magento"

    export MODULE_INSTALLER="${base}/scripts/build/module-installer"

    export MAGE_ADMIN_USER=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')
    export MAGE_ADMIN_PASSWORD=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')"123"

    if [ -z "${MYSQL_DATABASE+x}" ]; then
        export MYSQL_DATABASE=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 10 ; echo '')
        mysql -uroot -p"${MYSQL_ROOT_PASSWORD}" --execute="CREATE DATABASE ${MYSQL_DATABASE}"
    fi
    
    if [ -z "${MYSQL_USER+x}" ]; then
        export MYSQL_USER="root"
    fi

    if [ -n "${MYSQL_PASSWORD+x}" ]; then
        export MYSQL_PASSWORD_PROMPT=--password="${MYSQL_PASSWORD}"
    else
        export MYSQL_PASSWORD_PROMPT=""
    fi

    export DB_NAME=$MYSQL_DATABASE

    logvalue "DB Name: ${MYSQL_DATABASE}"
fi
